=========================
 Personalize your system
=========================

Installations
=============

::

   apt dist-upgrade
   apt install emacs
   apt install -y emacs mutt mercurial mu4e
   apt install -y emacs mutt mercurial mu4e g++ autotools-dev
   apt install -y emacs mutt mercurial mu4e g++ autotools-dev python3 python3-matplotlib
   apt install -y emacs mutt mercurial mu4e g++ autotools-dev python3 python3-matplotlib gnuplot ffmpeg texlive-latex-base texlive-latex-recommended python3-sphinx-rtd-theme texlive-latex-extra texlive-generic-extra texlive-fonts-recommended dot2tex biber latexmk librsvg2-bin plantuml python3-sphinxcontrib.plantuml python3-pip
   apt install -y emacs mutt mercurial mu4e g++ autotools-dev python3 python3-matplotlib gnuplot ffmpeg texlive-latex-base texlive-latex-recommended python3-sphinx-rtd-theme texlive-latex-extra texlive-generic-extra texlive-fonts-recommended dot2tex biber latexmk librsvg2-bin plantuml python3-sphinxcontrib.plantuml python3-pip compizconfig-settings-manager
   apt install bbdb
   apt install bbdb clementine
   apt install bbdb clementine fetchmail
   apt install postfix
   apt install dovecot-imapd dovecot-imapd dovecot-managesieved dovecot-sieve mutt w3m
   apt install aptitude

geeqie
finger
autotools-dev
autoconf
automake
libtool
ubuntu-restricted-extras ## to play mp3s



Configurations
==============

email
-----

I run my own mail server, so when I install I need to adapt that.  I
get mail with fetchmail (my .fetchmailrc is long established and it's
in my home directory, so I don't address it here).

But fetchmail needs to *deliver* the mail, so I install postfix (a
"mail transfer agent" (MTA), one of the modern heirs to the classic
sendmail program).

Postfix can deliver to the two different formats: mbox (one big file
with lots of emails in it) and maildir (a directory with many single
files with individual emails in them).

I use maildir format, so I use this command:

::

   sudo postconf -e "home_mailbox = Maildir/"

where the slash at the end of ``Maildir/`` means that it's maildir
format.

For dovecot, toward the top of ``/etc/dovecot/conf.d/10-mail.conf`` I
look for the ``mail_location`` line and change it to:

::

   mail_location = maildir:~/Maildir


Desktop environment tweaks
==========================

Here are a few things I do in the desktop environment when I get a new
machine going.  I sometimes use straight debian, sometimes mint,
sometimes ubuntu.

If I use the mate desktop on linux mint, together with compiz, then I
need to recover the Super_L key, since compiz uses it for the scale
plugin and in other places.

But mint by default uses the Super_L key for their top level menu.

To find and eliminate it I did:

::

   settings list-recursively | grep -i 'Super_L'
   # this shows that com.linuxmint.mintmenu is what's causing
   # the trouble with 
   gsettings set com.linuxmint.mintmenu hot-key ''
