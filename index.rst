.. Hacking Projects documentation master file, created by
   sphinx-quickstart on Sat Sep 16 07:52:48 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Sysadmin Hacks
==============

   :Date: |today|
   :Author: **Mark Galassi**

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   intro.rst
   personalize-your-system/personalize-your-system.rst
   simple-backups/simple-backups.rst
   ssh-explorations/ssh-explorations.rst
   network-basics/network-basics.rst
   network-probing/network-probing.rst
   cloud-backups/cloud-backups.rst
   modern-packaging/modern-packaging.rst
   packaging-python/packaging-python.rst
   making-VMs/making-VMs.rst
   container-basics/container-basics.rst
   services-with-containers/services-with-containers.rst
   cloud-VMs/cloud-VMs.rst
   collaboration-tools/collaboration-tools.rst
   installing-snipe-it/installing-snipe-it.rst
   installing-archivesspace/installing-archivesspace.rst
   splitting-audiobooks/splitting-audiobooks.rst
   learning-notmuch-email-system/learning-notmuch-email-system.rst
   old-unix-faqs/old-unix-faqs.rst
   app-how-to-build-the-book/app-how-to-build-the-book.rst
   app-license/app-license.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
