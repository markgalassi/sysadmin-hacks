=====================
 Motivation and plan
=====================

Systems administration has been a part of my life since I first
started using UNIX in 1983.  In these 40 years or so it has been an
important tool for doing research and in all other areas as well.

My intention is to teach some ways of doing everyday things with a bit
of command line expertise, a bit of programming, and the use of free
software tools.  The documentation here has a focus on systems work.
