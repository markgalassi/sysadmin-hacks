.. _sec-simple-backups:

================
 Simple backups
================

[status: barely started]

Motivation and plan
===================

Backups are arguably the most important thing in running a system.

Sometimes your system will come with a backup mechanism, but it's not
clear that you will understand how it all works.  This procedure has
very good control over where your backups are placed.

Procedure
=========

* One line setup, one line backup -- just one directory at this time.

.. code-block:: sh

   ## setup
   IMPORTANT_DIR=$HOME/Documents   ## or whatever you want to start with
   mkdir ~/rdiff

   ## every time you want to back up (i.e. daily):
   rdiff-backup -v 5 $IMPORTANT_DIR ~/rdiff/$IMPORTANT_DIR

* To examine what you have:

.. code-block:: console

   ## list what's available
   rdiff-backup --list-increments ~/rdiff/$IMPORTANT_DIR

* To show how you can accumulate more incrementals:

.. code-block:: console

   date --iso=seconds
   date --iso=seconds >> ~/Documents/timestamps
   rdiff-backup -v 5 $IMPORTANT_DIR ~/rdiff/$IMPORTANT_DIR
   date --iso=seconds >> ~/Documents/timestamps
   rdiff-backup -v 5 $IMPORTANT_DIR ~/rdiff/$IMPORTANT_DIR
   date --iso=seconds >> ~/Documents/timestamps
   rdiff-backup -v 5 $IMPORTANT_DIR ~/rdiff/$IMPORTANT_DIR

   ## this has created some more incremental backups; let's check them out:
   rdiff-backup --list-increments ~/rdiff/$IMPORTANT_DIR

   ## You should see an output that looks like this:

.. code-block:: text

   Found 2 increments:
     increments.2016-12-21T15:03:34-07:00.dir   Wed Dec 21 15:03:34  2016
     increments.2016-12-21T15:03:57-07:00.dir   Wed Dec 21 15:03:57  2016
   Current mirror: Wed Dec 21 15:04:08 2016

* To sync this to an off-site location.  NOTE: this is not yet the
  encrypted approach, so do not send private data with this method yet.

.. code-block:: sh

   YOUR_SERVER=my_server_hostname
   YOUR_REMOTE_LOGIN=$LOGNAME
   rsync -avz --delete ~/rdiff/ $YOUR_REMOTE_LOGIN@$YOUR_SERVER:rdiff
