#! /usr/bin/env python3

# for these plots you need the following on ubuntu 18.04:

# sudo apt install python3-numpy python3-matplotlib
# sudo apt install libgeos++-dev libgeos-dev libgdal-dev libproj-dev
# pip3 install cartopy geoplot h5py testresources
# pip3 install 'shapely==1.6.3'
# for some reason it does not work to "pip3 install crs", but things seem
# to work without (weird)

"""Simple mapping app, meant mostly to demonstrate cartopy ideas in
the context of mixing it with a flask-based web app.  Invoke it with

./map-app.py lon0 lat0 [lon1 lat1 [lon2 lat2 ...]]

and it will plot either a single point, or a progression of points
with lines joining them.

"""
import sys
import cartopy
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import argparse
import time

# # ax = plt.axes(projection=ccrs.PlateCarree())
# # ax = plt.axes(projection=ccrs.Mercator())
# ax = plt.axes(projection=ccrs.Mollweide())
# # ax = plt.axes(projection=ccrs.SouthPolarStereo())
# # ax = plt.axes(projection=ccrs.EquidistantConic())
# # ax = plt.axes(projection=ccrs.Sinusoidal())
# ax.stock_img()

ax = None

# ny_lon, ny_lat = -75, 43
# delhi_lon, delhi_lat = 77.23, 28.61
NZ_lon, NZ_lat = 174.763180, -36.852095 # Aukland, New Zeland
Sev_lon, Sev_lat = -5.995340, 37.388630 # Sevilla, Spain
lon_start, lat_start = NZ_lon, NZ_lat
lon_end, lat_end = Sev_lon, Sev_lat

def main():
    parser = argparse.ArgumentParser(description='plot an earth noise map file',
                                     epilog='example: plot-lut.py filname.h5 13e6 81e6 112e6')
    global args
    args = None
    # parse options
    do_options_parsing(parser)
    global ax
    projection_method = getattr(ccrs, args.projection)
    ax = plt.axes(projection=ccrs.Mollweide())
    # ax = plt.axes(projection=projection_method())
    ax.stock_img()
    # ax.add_feature(cartopy.feature.OCEAN)
    # # ax.coastlines()
    # ax.add_feature(cartopy.feature.COASTLINE)

    point_list = determine_point_list(args)

    if args.n_points <= 0 and len(point_list) == 0:
        save_file(plt, 'empty_map.svg')
        return 0

    # if len(sys.argv) == 1 or (len(sys.argv) == 2 and sys.argv[1] == '0'):
    #     # just dump an empty map
    #     save_file(plt, 'empty_map.svg')
    # elif len(sys.argv) == 2:
    #     n_iter = int(sys.argv[1])
    #     lon_progression = np.linspace(lon_start, lon_end, n_iter)
    #     lat_progression = np.linspace(lat_start, lat_end, n_iter)
    #     print(lon_progression)
    #     print(lat_progression)
    #     coord_progression = list(zip(lon_progression, lat_progression))
    #     plot_coord_progression(coord_progression)
    # elif len(sys.argv) == 3:    # single location
    #     id_str = 'single_point_%s_%s' % tuple(sys.argv[1:])
    #     plot_single_point((float(sys.argv[1]), float(sys.argv[2])))
    #     save_file(plt, 'saved_map_%s.svg' % id_str)
    # else:
    #     assert((len(sys.argv)-1) % 2 == 0) # even number of args
    #     lat_indices = [i for i in range(len(sys.argv[1:])) if i % 2 == 0]
    #     lon_indices = [i for i in range(len(sys.argv[1:])) if i % 2 == 1]
    #     coord_progression = [(float(sys.argv[1+i]), float(sys.argv[1+i+1])) 
    #                          for i in range(0, len(sys.argv[1:]), 2)]
    #     plot_coord_progression(coord_progression)
    plot_coord_progression(point_list)

def plot_single_point(pt):
    plt.plot([pt[0]], [pt[1]],
             color='blue', linewidth=5, marker='o',
             transform=ccrs.Geodetic())
    # draw_rect(pt, 20, 10, 'red')
    draw_poly(pt, (pt[0]+10, pt[1]+10), 'green')

def plot_coord_progression(progression):
    print('progression:', progression)
    # plot a line segment going from start to end
    plot_segment(progression[0], progression[-1])
    # describe the end points
    if len(progression) > 1:
        plt.text(lon_start - 3, lat_start - 12, 'Start',
                 horizontalalignment='right',
                 transform=ccrs.Geodetic())
        plt.text(lon_end + 3, lat_end - 12, 'End',
                 horizontalalignment='left',
                 transform=ccrs.Geodetic())
    # plot each point
    for i, pt in enumerate(progression):
        plot_single_point(pt)
        for ext in ('svg',):    # could also add 'png' or 'pdf'
            fname = 'saved_map_%d.' % i + ext
            save_file(plt, fname)

def plot_segment(p1, p2):
    """A "shortest line" between two points, plotted appropriately given
    the coordinate system."""
    (lon1, lat1) = p1
    (lon2, lat2) = p2
    plt.plot([lon1, lon2], [lat1, lat2],
             color='red', linewidth=4, linestyle='-',
             transform=ccrs.PlateCarree())


def draw_rect(center, width, height, color):
    ax.add_patch(mpatches.Rectangle(xy=(center[0]-width/2, center[1]-height/2), 
                                    width=width, height=height, facecolor=color,
                                    alpha=0.4, transform=ccrs.Geodetic()))

def draw_poly(center, pt0, color):
    pt0 = (center[0]-15, center[1])
    pt1 = (center[0], center[1]-15)
    pt2 = (center[0]+15, center[1])
    pt3 = (center[0], center[1]+15)
    poly = np.array([pt0, pt1, pt2, pt3])
    ax.add_patch(mpatches.Polygon(xy=poly, facecolor=color,
                                    alpha=0.4, transform=ccrs.Geodetic()))


def save_file(the_plot, fname):
    the_plot.savefig(fname)
    print('COMMUNICATION_FILE_TO_PLOT: %s' % fname)
    sys.stdout.flush()
    sys.stdout.flush()


def do_options_parsing(parser):
    """
    Parse command line arguments

    Parameters
    ==========

    Returns
    =======
    does not return anything; just modifies the global args variable
    """
    global args
    # parser.add_argument('--empty', default=False, action='store_true',
    #                     help='Just generate an empty world image.')
    parser.add_argument('-f', '--coord-file', default='', 
                        help='File with list of "lon lat" coordinates')
    parser.add_argument('-p', '--point', nargs=2, type=float,
                        help='lon and lat coordinates of a point on earth.')
    parser.add_argument('-n', '--n-points', default=-1, type=int,
                        help='How many points will we try draw on this itinerary.')
    parser.add_argument('-P', '--projection', default='Mollweide',
                        # choices=['Mollweide', 'PlateCarree', 'Mercator', 'Sinusoidal',
                        #          'random'])
                        choices=['random', 'AlbersEqualArea', 'AzimuthalEquidistant',
                                 'EckertI', 'EckertII', 'EckertIII', 'EckertIV', 'EckertV', 
                                 'EckertVI', 'EqualEarth', 'EquidistantConic', 'EuroPP',
                                 'GOOGLE_MERCATOR', 'Geostationary', 'Gnomonic',
                                 'InterruptedGoodeHomolosine', 'LambertAzimuthalEqualArea',
                                 'LambertConformal', 'LambertCylindrical', 'Mercator',
                                 'Miller', 'Mollweide', 'NearsidePerspective',
                                 'NorthPolarStereo', 'OSGB', 'OSNI', 'Orthographic',
                                 'PlateCarree', 'Projection', 'Robinson', 'RotatedPole',
                                 'Sinusoidal', 'SouthPolarStereo', 'Stereographic',
                                 'TransverseMercator', 'UTM'])

    parser.add_argument('-v', '--verbose', default=False, action='store_true',
                        help='Enables more messages to standard out')
    args = parser.parse_args()
    # some consistency checks and post-parsing actions
    if args.verbose:
        print('Verbose!')
        configure_cmd = '%s --quiet' % configure_cmd_base
        yum_cmd = 'yum --quiet'
    if args.coord_file and args.point:
        sys.stderr.write('Error: you cannot specify --coord-file and also --point.'
                         + ' It is one xor the other.\n')
        sys.exit(1)


def determine_point_list(args):
    ptlist = []
    if args.point:
        ptlist.append((args.point[0], args.point[1]))
    if args.coord_file:
        with open(args.coord_file, 'r') as f:
            for line in f:
                if line.startswith('#'):
                    continue
                pt = tuple([float(w) for w in line.split()])
                ptlist.append(pt)
    if len(ptlist) == 0 and args.n_points > 0:
        # if nothing else made points, generate a trajectory here
        n_iter = args.n_points
        lon_progression = np.linspace(lon_start, lon_end, n_iter)
        lat_progression = np.linspace(lat_start, lat_end, n_iter)
        print(lon_progression)
        print(lat_progression)
        ptlist = list(zip(lon_progression, lat_progression))
    return ptlist
        



if __name__ == '__main__':
    main()
