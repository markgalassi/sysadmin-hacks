# from https://maxhalford.github.io/blog/flask-sse-no-deps/
# The author Max Halford distributes this code under the MIT license.

import queue

class MessageAnnouncer:
    """This class is used to form a queue of messages being sent by a
    flask server to a javascript web client.  This goes together with
    the mechanism in app.py, in the function start_SSE_stream(), and
    the helper function trigger_file_is_read().
    """
    def __init__(self):
        self.listeners = []

    def listen(self):
        q = queue.Queue(maxsize=400)
        self.listeners.append(q)
        return self.listeners[-1]

    def announce(self, msg):
        for i in reversed(range(len(self.listeners))):
            try:
                self.listeners[i].put_nowait(msg)
            except queue.Full:
                del self.listeners[i]
        print('queue:', self.listeners)


def format_sse(data: str, event=None) -> str:
    """Utility function to format the message for server-sent event (SSE)
    transmission"""
    msg = f'data: {data}\n\n'
    if event is not None:
        msg = f'event: {event}\n\n{msg}'
    return msg

