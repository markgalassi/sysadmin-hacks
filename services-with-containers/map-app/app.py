#! /usr/bin/env python3

# from tutorial at:
# https://runnable.com/docker/python/dockerize-your-flask-application
# but I corrected the incorrect python code in it.

# flask_web/app.py

import os
import subprocess
import time

import flask
from flask import Flask, render_template, request, redirect, url_for
from flask import flash, session, g, jsonify, send_from_directory

from MessageAnnouncer import MessageAnnouncer, format_sse

announcer = MessageAnnouncer()

app = Flask(__name__)
app.secret_key = 'TrulySecret'

image_list = []

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/')
def index():
    app.config['UPLOAD_EXTENSIONS'] = ['.jpg', '.png', '.gif', '.txt', '.dat', '.xml', '.yml']
    print(app.config['UPLOAD_EXTENSIONS'])
    return render_template('index.html')


@app.route('/index.html') # a trick to make sure that localhost:5000/index.html works
def explicit_index_html():
    return redirect(url_for('index'))


@app.route('/start_SSE_stream', methods=['GET'])
def start_SSE_stream():
    session['username'] = 'dudername'
    def stream():
        messages = announcer.listen()  # returns a queue.Queue
        i = 0
        while True:
            msg = messages.get()  # blocks until a new message arrives
            i += 1
            yield msg

    return flask.Response(stream(), mimetype='text/event-stream')


@app.route('/_do_single_point')
def do_single_point():
    lon = request.args.get('longitude', 0, type=float)
    lat = request.args.get('latitude', 0, type=float)
    session['username'] = 'dudername'
    prog = ['./map-app.py', '--point', f'{lon}', f'{lat}']
    result_img = run_external_prog(prog)
    return jsonify(result_img)

@app.route('/_do_with_file_input')
def do_with_file_input():
    fname = request.args.get('file', 'coord_progression.dat', type=str)
    session['username'] = 'dudername'
    prog = ['./map-app.py', '--coord-file', f'{fname}']
    result_img = run_external_prog(prog)
    return jsonify(result_img)

@app.route('/_do_sequence_run')
def do_sequence_run():
    n_points = request.args.get('n_points', 0, type=int)
    session['username'] = 'dudername'
    prog = ['./map-app.py', '--n-points', f'{n_points}']
    result_img = run_external_prog(prog)
    return jsonify(result_img)

@app.route('/_get_n_images')
def get_n_images():
    print('about to return:', len(image_list))
    return jsonify(result=len(image_list))


def run_external_prog(prog):
    """Run the external program, in our case usually ./map-app.py, with
    arguments.  prog is a list: [prog_path, arg1, arg2, ...]"""
    print('prog:', prog)
    run_pipe = subprocess.Popen(prog, stdout=subprocess.PIPE)
    run_output = run_pipe.communicate()[0]
    global image_list
    image_list = []
    oh = ''                     # output html
    oh += '<p>ran %s!</p>' % (prog.__str__())
    for line in run_output.decode('utf-8').split('\n'):
        if line[:len('COMMUNICATION_')] == 'COMMUNICATION_':
            print(f'COMMUNICATION_: {line} at', time.time())
            image_fname, image_html_code = handle_communication_line(line)
            trigger_file_is_ready(image_fname) # send interim info to javascript client
            time.sleep(1.0/24.0)               # FIXME: deliberate slowdown for testing
            flash('Generating ' + image_fname, 'info')
            image_list.append(image_fname)
            oh += image_html_code
    oh += '<pre>' + run_output.decode('utf-8') + '</pre>'
    if len(image_list) > 0:
        return image_list[-1]       # return the last image found
    else:
        return None


def handle_communication_line(line):
    """This is a helper function to run_external_prog().  It takes a line
    that has been output by a helper program and scans it for a
    special format that indicates that it needs to be parsed and
    something needs to be done with it.
    """
    oh = ''                     # output html
    plot_prefix = 'COMMUNICATION_FILE_TO_PLOT:'
    if line[:len(plot_prefix)] == plot_prefix:
        fname = line.split()[1]
        os.makedirs('static', exist_ok=True)
        if os.path.exists(os.path.join('static', fname)):
            os.unlink(os.path.join('static', fname))
        new_fpath = os.path.join('static', fname)
        os.rename(fname, new_fpath)
        oh = """\n<img src="/static/%s" width="200" alt="%s">\n""" % (fname, fname)
    return new_fpath, oh


def trigger_file_is_ready(image_fname):
    msg = format_sse(data=image_fname, event='file_ready')
    # print(f'TRIGGER: about to send an SSE message to the client: <{msg}>')
    announcer.announce(msg=msg)
