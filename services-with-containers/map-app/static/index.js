// the next lines try to load jquery using a recipe for loading js
// from js.  this is notoriously weird and it's not clear that it
// works, but at this time the file index.html also loads jquery.
var script = document.createElement("script");
script.src = "static/jquery.js";
script.type = "text/javascript";
document.getElementsByTagName("head")[0].appendChild(script);

$(function() { console.log("loading index.js"); });

$(function () { 
    var source = new EventSource("/start_SSE_stream");
    console.log("after loading page I started /start_SSE_stream");

    source.onMessage = function(e) {
        console.log("dude, got message");
        console.log(e);
    }

    source.onmessage = function(e) {
        console.log("source.onmessage");
        console.log(e);
    }

    source.addEventListener("greeting", function(e){
        console.log("Received a greeting:", e.data);
        var data = JSON.parse(e.data);
        alert("Server says " + data.message);
    });

    source.addEventListener("message", function(e){
        console.log("Received a message:");
        console.log(e);
        // The image was sometimes being cached.  This trick from
        // https://stackoverflow.com/a/2104998/693429 fixes it.
        d = new Date();
        img_url = e.data + "?" + d.getTime() + d.getMilliseconds();
        $("#current_map img").attr("src", img_url);
        console.log("JUST_SET: img URL to: " + img_url);
    });

    source.addEventListener("open", function(e) {
        // Connection was opened.
        console.log("connection was opened");
        console.log(e);
        console.log(e.data);
    }, false);

    source.addEventListener("error", function(e) {
        if (e.readyState == EventSource.CLOSED) {
            // Connection was closed.
            console.log("**error** connection was closed");
        }
    }, false);
});


// this function is called in response to a button click in the
// RunOnFile button.  It sends a message to the server at URL
// /_do_with_file_input.  The response is an image URL, so we set our
// image element to have that URL as an "src" attribute.
function run_with_file_input()
{
    console.log("run_with_file_input");
    console.log($('#file').val());
    // the jquery $.getJSON(...) call sends a message to a URL, and
    // then invokes the function right after when the URL returns.
    var submitURL = "/_do_with_file_input";
    $.getJSON($SCRIPT_ROOT + submitURL, {
        n_points: $('#file').val()
    }, function(data) {
        // this function is called when the URL returns its information
        console.log("DONE " + submitURL + " -- data:");
        console.log(data);
        // Now we display the image by setting the "src" attribute to
        // the URL for the image.  The image sometimes gets cached, so
        // this trick from https://stackoverflow.com/a/2104998/693429
        // fixes it: make the URL unique by adding a ?TIME_STRING
        img_url = data + "?" + (new Date()).getTime();
        $("#current_map img").attr("src", img_url);
        console.log("JUST_SET: img attribute " + data);
    });
}


// this function is called in response to a button click in the
// SequenceRun button.  It sends a message to the server at URL
// /_do_sequence_run.  The response is an image URL, so we set our
// image element to have that URL as an "src" attribute.
function run_sequence_run()
{
    console.log("run_sequence_run");
    console.log($('#n_points').val());
    var submitURL = "/_do_sequence_run";
    $.getJSON($SCRIPT_ROOT + submitURL, {
        n_points: $('#n_points').val()
    }, function(data) {
        // this function is called when the URL returns its information
        console.log("DONE " + submitURL + " -- data:");
        console.log(data);
        // Now we display the image by setting the "src" attribute to
        // the URL for the image.  The image sometimes gets cached, so
        // this trick from https://stackoverflow.com/a/2104998/693429
        // fixes it: make the URL unique by adding a ?TIME_STRING
        img_url = data + "?" + (new Date()).getTime();
        $("#current_map img").attr("src", img_url);
        console.log("JUST_SET: img attribute " + data);
        // console.log("DONE SINGLE IMAGE, data:");
        // console.log(data);
        // // The image was sometimes being cached.  This trick from
        // // https://stackoverflow.com/a/2104998/693429 fixes it.
        // d = new Date();
        // img_url = data + "?" + d.getTime() + d.getMilliseconds();
        // $("#current_map img").attr("src", img_url);
        // console.log("JUST_SET: img attribute " + data);
    });
}


// this function is called in response to a button click in the
// SinglePoint button.  It sends a message to the server at URL
// /_do_single_point.  The response is an image URL, so we set our
// image element to have that URL as an "src" attribute.
function run_single_point()
{
    console.log("run_single_point");
    var submitURL = "/_do_single_point";
    $.getJSON($SCRIPT_ROOT + submitURL, {
        longitude: $('input[name="longitude"]').val(),
        latitude: $('input[name="latitude"]').val()
    }, function(data) {
        // note that at this time I don't process the returned
        // information because it's processed in the SSE stream
        console.log("DONE " + submitURL + " -- data:");
        console.log(data);
    });
}



$(function() {             // some startup actions to get an empty map
    $("#n_points").val(0);
    run_sequence_run();
    $("#n_points").val(10);
    $("#n_points").val(10);
});
