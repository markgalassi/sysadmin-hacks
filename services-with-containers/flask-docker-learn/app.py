#! /usr/bin/env python3

# from tutorial at:
# https://runnable.com/docker/python/dockerize-your-flask-application
# but I corrected the incorrect python code in it.

# flask_web/app.py

from flask import Flask
app = Flask(__name__)

import subprocess

@app.route('/')
def hello_world():
    uname_pipe = subprocess.Popen('uname -a'.split(), stdout=subprocess.PIPE)
    uname_output = uname_pipe.communicate()[0]
    return ('<p>Hey, we have Flask in a Docker container!</p>'
            + '\n<p>' + uname_output.decode('utf-8') + '</p>')

@app.route('/run/')
def run():
    return '<p>I should run the C program!</p>'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
