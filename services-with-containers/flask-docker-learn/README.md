From a tutorial at:
https://runnable.com/docker/python/dockerize-your-flask-application
the tutorial is mostly good but it has some bogus python code, so look
here in app.py to see the correct code.

Another tutorial is at:
https://medium.com/@tasnuva2606/dockerize-flask-app-4998a378a6aa

To make it work:

Use the Dockerfile in this directory like this:

docker build -t flask-docker-learn:latest .

docker run -d -p 5001:5000 flask-docker-learn

Then you can open up a web browser and connect to http://localhost:5001
