.. _sec-splitting-audiobooks:

======================
 Splitting audiobooks
======================

Motivation and plan
===================

Audiobooks are nice.  Many are available on youtube.  How do you
download them and split them into reasonable mp3 chunks to play in a
car or on a portable device?

Single big file is a problem because of fast-forward and rewind.  You
need to keep context.

The ideal way to split an audiobook 

Finding an online audiobook
===========================

.. code:: bash

   mkdir ~/Music/audiobooks
   cd ~/Music/audiobooks
   youtube-dl -t 'https://www.youtube.com/watch?v=KxEFNy7K44Q'

