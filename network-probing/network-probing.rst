===================================
 How to get insight into a network
===================================

Here we go beyond the network basics and learn how to use commands
that automatically probe your local network.

New commands here are: "nmap", "telnet host portnumber", "host", "ssh"


Before we start
===============

* Read the previous chapter on networking basics.

* make sure you have the ssh daemon installed:

.. code-block:: console

   $ sudo apt-get install openssh-server

* hook up to wired and try these things, then do the same with wireless

interfaces: the network devices our packets go through to get out
=================================================================

To get your bearings on how you are getting out to the world at large
you can run:

.. code-block:: console

   $ ip route
   default via 172.20.20.1 dev wlp2s0 proto dhcp metric 600 
   169.254.0.0/16 dev virbr0 scope link metric 1000 linkdown 
   172.20.20.0/24 dev wlp2s0 proto kernel scope link src 172.20.20.226 metric 600 
   192.168.122.0/24 dev virbr0 proto kernel scope link src 192.168.122.1 linkdown 

and look at where the "default" route is.  This tells me that the
default device as I ran this command (on a laptop, while visiting a
school) was wlp2s0.  The prefix "wl" means "wireless lanl"
(i.e. wifi), and the IP address they gave me probably starts with
172.20.20

.. code-block:: console

   $ ip addr # -- what network interfaces do we have?

(in some situations you might have to run "/sbin/ifconfig" if /sbin is
not in your PATH variable).  Here is a typical output, with some
analysis:

.. code-block:: console

   $ ip addr
   1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
       link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
       inet 127.0.0.1/8 scope host lo
          valid_lft forever preferred_lft forever
       inet6 ::1/128 scope host
          valid_lft forever preferred_lft forever
   2: enp0s31f6: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
       link/ether a4:4c:c8:42:b4:9e brd ff:ff:ff:ff:ff:ff
   3: wlp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
       link/ether 28:c6:3f:ec:0b:e4 brd ff:ff:ff:ff:ff:ff
       inet 172.20.20.226/24 brd 172.20.20.255 scope global dynamic noprefixroute wlp2s0
          valid_lft 257519sec preferred_lft 257519sec
       inet6 fe80::59d1:e0ef:2d22:6ece/64 scope link noprefixroute
          valid_lft forever preferred_lft forever

"lo" is the loopback interface.  It comes up with the ip address
127.0.0.1 (or sometimes something else with 127.).  It is usually
associated with the hostname "localhost" (which you can find in
/etc/hosts).  It is a "loopback" which means that if you do something
like "ssh localhost" or "ssh 127.0.0.1" and log in to your machine.
It will act as if it had gone across a network, but the bits just
looped around in memory instead of going onto the ethernet or wifi.

You won't use "lo" much, but things like testing ssh without a full
network can be useful.

Then we have enp0s31f6 (or eth0 or wlan0.... and so forth).

The most useful one for most people nowadays is the wireless interface
that starts with "wl":

::

   3: wlp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
       link/ether 28:c6:3f:ec:0b:e4 brd ff:ff:ff:ff:ff:ff
       inet 172.20.20.226/24 brd 172.20.20.255 scope global dynamic noprefixroute wlp2s0
          valid_lft 257519sec preferred_lft 257519sec
       inet6 fe80::59d1:e0ef:2d22:6ece/64 scope link noprefixroute
          valid_lft forever preferred_lft forever

This is where you start to see what your network looks like.  The
first thing to look at the "inet addr".  In a typical home network it
will be 10.x.x.x or 192.168.x.x.  To better understand these "special
ranges" you can look at the "Reserved IP addresses" article in
wikipedia:

https://en.wikipedia.org/wiki/Reserved_IP_addresses

In this particular case I ran the command when I was at a facility
where the default IP address they gave out started with 172.20.20, and
I have 172.20.20.226.

A lot of gadgets that offer a local private network (like an "access
point" or a home DSL/cable-modem router) give out addresses of the
192.168.x.x or 10.x.x.x types.

At this time I ignore the "inet6 addr" field: I have not yet studied
up on ipv6 addresses and for a bit longer the topic can be ignored.

If the "inet addr" field is not there for any ethX or wlanX interface
then you have not been offered a dynamic IP address by a DHCP server.

This is usually a problem: the aforementioned home routers all have
DHCP by default and something could be broken.  But sometimes it just
means that the network admin wants you to set up the interface by
hand, possibly with a static ip address.

Once you see if you have a 10.x.x.x or 192.168.x.x address you can
explore the various other things installed on that network.  All the
various hosts will have addresses that start with the same thing and
look like your address.

routing: how to get from here to a remote host
==============================================

.. code-block:: console

   $ ip route

which will quickly return the way your network traffic is routed to
the outside world.  You can ten use the ``route`` command to create
special routes for some of your traffic.

The first thing to do is see where the "default" route (0.0.0.0) goes.
The typical DHCP configuration will set all this up for you and it
will look like this:

.. code-block:: console

   $ ip route
   default via 172.20.20.1 dev wlp2s0 proto dhcp metric 600 
   169.254.0.0/16 dev virbr0 scope link metric 1000 linkdown 
   172.20.20.0/24 dev wlp2s0 proto kernel scope link src 172.20.20.226 metric 600 
   192.168.122.0/24 dev virbr0 proto kernel scope link src 192.168.122.1 linkdown 

which means that all your outgoing traffic will go through 192.168.0.1
which (in a typical home situation) is the address of your DSL/cable
modem modem.

You can read that output, in English, as: "default traffic is routed
through device wlp2s0 and is sent through gateway 172.20.20.0".

Routing becomes especially interesting when you have a tunnel of some
sort, like a VPN.  There are several ways of configuring it, but often
the VPN shows up as a new interface called "ppp0", which you would
notice by typing "ifconfig".  When you have a VPN tunnel the routing
table might look like this:

::

   $ route -n
   Kernel IP routing table
   Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
   0.0.0.0         192.168.0.56    0.0.0.0         UG    100    0        0 ppp0
   192.168.0.0     192.168.0.1     0.0.0.0         UG    100    0        0 eth0
   192.168.0.57    192.168.0.1     0.0.0.0         UG    100    0        0 eth0
   [FIXME: just made this up; must get from real VPN situation at home]

You can read this as: "Default traffic now goes through the ppp0
device (this is a "virtual" device) over to the other end of the
tunnel, which then sends it out to the net.  Local traffic to my
private subnet 192.168.x.x will still go through the eth0 device."

changing routes: choosing special routes to reach some hosts or nets
====================================================================

Let's say that your VPN takes you into a remote network that has your
same private subnet 192.168.x.x

This becomes "interesting": if you want to get to 192.168.0.180 on the
remote subnet, it won't!  It will go to 192.168.0.180 on your own
subnet.  This is unfortunate, but you can get around it by adding a
special route for 192.168.0.180:

::

   $ sudo route add -host 192.168.0.180 dev ppp0

Let's also say that you are unhappy with all your traffic going
through the ppp0 interface -- it's kind of slow, so you decide to
route your traffic to youtube and netflix through eth0 so it goes
straight through instead of hopping to the VPN server in between:

::

   $ sudo route add -host youtube.com dev eth0
   $ sudo route add -host netflix.com dev eth0

You could also reclaim all of your default traffic as going through
eth0 and only use ppp0 for traffic into your company's internal
network:

$ sudo route add -net default dev eth0

Of course you should run "route" or "route -n" after each of these
modifications so that you can see what happened.


traceroute: what gateways do you pass going from here to there?
===============================================================

Do you ever wonder where your packets hop between your computer and a
google query?  Try this:

::

   $ traceroute --resolve-hostnames google.com
   traceroute to google.com (142.250.72.46), 64 hops max
     1   192.168.1.1 (_gateway)  1.623ms  1.071ms  1.350ms
     2   100.92.207.66 (100.92.207.66)  10.993ms  11.768ms  11.244ms
     3   96.216.21.185 (96.216.21.185)  9.811ms  12.140ms  11.907ms
     4   162.151.13.129 (be-2-ar01.albuquerque.nm.albuq.comcast.net)  13.574ms  18.082ms  12.507ms
     5   162.151.13.129 (be-2-ar01.albuquerque.nm.albuq.comcast.net)  11.996ms  11.389ms  10.800ms
     6   96.110.44.29 (be-36841-cs04.1601milehigh.co.ibone.comcast.net)  20.337ms  23.803ms  19.478ms
     7   96.110.44.25 (be-36831-cs03.1601milehigh.co.ibone.comcast.net)  20.549ms  20.737ms  21.852ms
     8   50.248.118.30 (50.248.118.30)  20.104ms  23.102ms  19.036ms
     9   50.248.118.30 (50.248.118.30)  29.428ms  19.661ms  21.525ms
    10   172.253.75.177 (172.253.75.177)  21.970ms  21.151ms  21.952ms
    11   209.85.142.171 (209.85.142.171)  21.089ms  21.041ms  22.310ms
    12   142.250.72.46 (den16s08-in-f14.1e100.net)  21.356ms  19.599ms  20.845ms

This shows that from where I was logged in (web3.rdrop.com) the
packets went through a variety of hops to then get from my host to the
google search server.  It also tells you the delay associated with
each hop, measured in milliseconds.

Traceroute is wonderful when it works, but some network managers turn
it off because it can give outsiders insight into their internal
network, so sometimes it will not work.

You can also try the modern replacement for traceroute called ``mtr``

ping: see if a host is alive
============================

You can find out if a host is up and how fast the network is to it
with "ping":

::

   $ ping google.com
   PING google.com (142.250.72.14) 56(84) bytes of data.
   64 bytes from den08s06-in-f14.1e100.net (142.250.72.14): icmp_seq=1 ttl=114 time=19.1 ms
   64 bytes from den08s06-in-f14.1e100.net (142.250.72.14): icmp_seq=2 ttl=114 time=19.0 ms
   64 bytes from den08s06-in-f14.1e100.net (142.250.72.14): icmp_seq=3 ttl=114 time=19.2 ms
   64 bytes from den08s06-in-f14.1e100.net (142.250.72.14): icmp_seq=4 ttl=114 time=18.9 ms
   [...]

Sometimes network managers turn off the ability to ping through their
gateways.

nmap: what hosts are on this network? and what services do they offer?
======================================================================

nmap has many options and can do just about anything to scout out a
network.  Here I will show you how to answer the question:

"What hosts are on this network and what services are they offering?"

First note that a "service" is something like "outgoing mail" (smtp)
or "email access" (imap) or "incoming logins" (ssh) or "web server"
(http) or "remote desktop" (rdp, ms-term-serv).

Each service is associated with a port on the host that offers that
service.  The English language narrative is, for example: "to read
your email with IMAP you connect to port 993 of host host
192.168.122.1".

So here is one of the most basic nmap invocations which (thanks to the
/24 on the command line) will search the range of hosts 192.168.122.0
to 192.168.122.255:

.. code-block:: console

   $ sudo apt install nmap
   $ # ...
   $ sudo nmap -sn 192.168.1.0/24
   Starting Nmap 7.80 ( https://nmap.org ) at 2022-10-12 07:24 MDT
   Nmap scan report for _gateway (192.168.1.1)
   Host is up (0.00022s latency).
   MAC Address: B0:95:75:45:A2:21 (Tp-link Technologies)
   Nmap scan report for sarastro (192.168.1.174)
   Host is up (0.00045s latency).
   MAC Address: DC:4A:3E:98:23:5A (Hewlett Packard)
   Nmap scan report for 192.168.1.206
   Host is up (0.083s latency).
   MAC Address: EE:91:79:EA:B7:36 (Unknown)
   Nmap scan report for magicflute-wifi (192.168.1.220)
   Host is up (0.081s latency).
   MAC Address: D8:FC:93:81:B4:2E (Intel Corporate)
   Nmap scan report for 192.168.1.234
   Host is up (0.080s latency).
   MAC Address: F8:5E:A0:8D:9E:B0 (Unknown)
   Nmap scan report for dongiovanni (192.168.1.142)
   Host is up.
   Nmap scan report for dongiovanni (192.168.1.215)
   Host is up.
   Nmap done: 256 IP addresses (7 hosts up) scanned in 2.43 seconds


.. code-block:: console

   $ nmap 192.168.1.0/24

   Starting Nmap 5.51 ( http://nmap.org ) at 2016-08-30 10:13 MDT
   Nmap scan report for mozart (192.168.122.1)
   Host is up (0.0036s latency).
   Not shown: 994 closed ports
   PORT     STATE SERVICE
   22/tcp   open  ssh
   25/tcp   open  smtp
   53/tcp   open  domain
   143/tcp  open  imap
   993/tcp  open  imaps
   3389/tcp open  ms-term-serv

   Nmap scan report for c6b (192.168.122.33)
   Host is up (0.0042s latency).
   Not shown: 997 closed ports
   PORT    STATE SERVICE
   22/tcp  open  ssh
   80/tcp  open  http
   111/tcp open  rpcbind

   Nmap scan report for c6r (192.168.122.210)
   Host is up (0.0033s latency).
   Not shown: 997 closed ports
   PORT    STATE SERVICE
   22/tcp  open  ssh
   80/tcp  open  http
   111/tcp open  rpcbind

   Nmap done: 256 IP addresses (3 hosts up) scanned in 2.94 seconds

This is a mouthful but is easy to read.  You see that 192.168.122.1 is
offering (among others) ssh, SMTP, IMAP and RDP services on ports 22,
25, 993 and 3389 respectively.


nmap: a script to make a visual map
===================================

You can write a program to parse the output of nmap on a network and
generate a visual map of the network.

Let us start with a simple version.  Download the
:download:`map_network_utils.py` program and try running it.

This program, when run on a typical home router, generates the file
``netmap__192.168.0.1_24.svg`` shown in


.. _fig-nmap__192.168.1.1_24-simple:

.. figure:: netmap__192.168.1.1_24.*

   First example of a graph generated from nmap's output on a typical
   home router situation.

There is a great wealth of possibilities to make these graphs more
attracive and more interesting.  For example you can use nmap and
pnscan to get more information and then study the graphviz gallery at
https://graphviz.org/gallery/ to look for interesting visualization
options.  Please contact me if you would like to work on such a
project.

A further exercise would be to write a program which dynamically scans
the network at certain intervals, and uses a graphical interface to
show the network topology.

pnscan: looking at open ports
=============================

::

   $ sudo pnscan 192.168.1.211 1:90
   192.168.1.211   :    22 : TXT : SSH-2.0-OpenSSH_8.2p1 Ubuntu-4ubuntu0.1\r\n
   192.168.1.211   :    25 : TXT : 220 magicflute.galassi.org ESMTP Postfix (Ubuntu)\r\n
   192.168.1.211   :    25 : TXT : 220 magicflute.galassi.org ESMTP Postfix (Ubuntu)\r\n
   192.168.1.211   :    22 : TXT : SSH-2.0-OpenSSH_8.2p1 Ubuntu-4ubuntu0.1\r\n

::

   $ sudo pnscan 192.168.1.0/24 1:90
   192.168.1.1     :    22 : TXT : SSH-2.0-dropbear_2011.54\r\n
   192.168.1.1     :    22 : TXT : SSH-2.0-dropbear_2011.54\r\n
   192.168.1.134   :    22 : TXT : SSH-2.0-OpenSSH_8.2p1 Ubuntu-4ubuntu0.1\r\n
   192.168.1.174   :    22 : TXT : SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.3\r\n
   192.168.1.198   :    22 : TXT : SSH-2.0-OpenSSH_8.2p1 Ubuntu-4ubuntu0.1\r\n
   192.168.1.211   :    22 : TXT : SSH-2.0-OpenSSH_8.2p1 Ubuntu-4ubuntu0.1\r\n
   192.168.1.211   :    25 : TXT : 220 magicflute.galassi.org ESMTP Postfix (Ubuntu)\r\n

Now try it with more ports, like 1:1024



telnet: an ancient command with an interesting modern use
=========================================================

The original ARPAnet had three applications in the early 1970s: email,
telnet (log in to a remote machine), and ftp (file transfer protocol).

telnet is no longer used to log in to hosts because it does not
encrypt passwords or the data stream (use ssh instead), but it turns
out to be very useful to understand and debug services.  You can
telnet into a specific port on a host and even try typing protocol
strings at it:

::

   $ telnet 192.168.122.1 imap  ## or telnet 192.168.122.1 143
   Trying 192.168.122.1...
   Connected to 192.168.122.1.
   Escape character is '^]'.
   OK [CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE STARTTLS LOGINDISABLED] Dovecot ready.

Note that one almost always uses the "imaps" port (993) because it
encrypts passwords and text, but it is harder or impossible to type
valid IMAP protocol strings when the server is expecting encrypted
information!

::

   $ telnet 192.168.122.1 imaps  ## or telnet 192.168.122.1 993
   Trying 192.168.122.1...
   Connected to 192.168.122.1.
   Escape character is '^]'.

This can be done for all the ports, but there will only be a few
protocols with which you can have a meaningful conversation typing
protocol strings at it.  All in all it's mostly a way of checking that
those services are working well.

And for some fun: in the old days lots of servers were set up to
respond to a telnet command.  Early weather servers, creative "ascii
art" animations, interactive games, ... were available by running
``telnet hostname``.

Most of those are now gone or hard to access from most points, but
some are still around.  The site ``telehack.com`` has collected
several of the classic telnet-served animations and games.

Try:

::

  $ telnet telehack.com

You will be prompted with a list of games and animations.  You can try
just typing ``pong``, but for an amazing display try typing
``starwars``.

::

                                                 /~\
                    And don't let me catch      |oo )
                       you following me         _\=/_
                       begging for help.    #  /  _  \  #
                                            \\//|/.\|\\//
                                             \/  \_/  \/
                                                |\ /|
                                                \_ _/
                                                | | |
                                                | | |
                                                []|[]
                                                | | |
          \____________________________________/_]_[_\______________________





host: the Domain Name System
============================

We think of hosts on a network as having names, but the routing system
uses their ip address to get to them.  The "domain name service" (DNS)
translates from name to ip address.  This is also sometimes called
"resolving a name".  Each host has to know who offers it "name
service".  This is usually set up when you connect to DHCP.  The file
``/etc/resolv.conf`` gets created with information on what your name
servers are:

::

   $ cat /etc/resolv.conf
   search rdrop.com
   nameserver 69.59.192.61
   nameserver 69.59.192.62

This means that when you connect to "google.com", the computer will
ask host 69.59.192.61 what google.com's IP address is, and then the
connection can proceed.

Note that nowadays the ``/etc/resolv.conf`` file tends to look more
complicated: it is created dynamically when you connect to a network,
using information returned by your network's router.  But it should
still specify which nameservers your system is using.

There is a command called "host" which queries the nameserver and
tells you what it found about a given hostname.  Try the following:

::

   $ host google.com
   $ host gmail.com
   $ host rdrop.com
   $ host mit.edu

Note that this also tells you which host will handle email going to
that domain: this is usually a dedicated machine.  Email was an early
internet service, so there is a special DNS record for that, called
the "MX record" (MX stands for "mail exchange[r]").

ssh: logging in to another computer
===================================

ssh is a candidate for "most amazing program ever written" in the
small category.  It allows you to log in to a remote host, encrypting
all traffic.  It also allows you to use the "agent" mechanism so that
you don't have to use a password every time.  It then lets you tunnel
connections to certain ports through the ssh connection, and there the
fun gets out of hand.  There is also a way of tunneling your web
traffic through an ssh connection.

Here are some simple examples, keeping in mind that you need to log in
to a host that is running the ssh service (port 22).

$ telnet 192.168.122.1 22
   verifies that someone is indeed listening on ssh's port 22.
$ ssh localhost
   logs in to this host with the loopback interface

[FIXME: unfinished set of examples]
