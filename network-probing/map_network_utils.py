#! /usr/bin/env python3

"""Probe a local network (selected with the variable map_target_net
below) and make a graph to visualize it."""

import subprocess
import pprint
import sys
try:
    from natsort import natsorted
except:
    print('please install package python3-natsort')
    sys.exit(1)
try:
    import pygraphviz as pgv
except:
    print('please install package python3-pygraphviz')
    sys.exit(1)

def main():
    map_cmd = 'nmap'
    ## set the -O option for the fancy guessing on what operating
    ## system it is
    # map_options = ['-O', '-n', '-F']
    map_options = ['-F', '-n']
    ## here I set which network I am probing; in the comments you will
    ## see a few options
    # map_target_net = '192.168.0.1/24' # for home router subnets at 192.168.0
    map_target_net = '192.168.1.1/24' # for home router subnets at 192.168.1
    # map_target_net = '192.168.122.1/24' # for virt-manager subnets
    # map_target_net = '128.165.44.1/24'   # try a typical work class C network
    # map_target_net = get_ip()+'/24'   # guess my externally visible IP address
    out_fname = 'netmap' + '__' + map_target_net.replace('/', '_') + '.dot'

    ## start by running the nmap command and capturing its output into
    ## a dictionary
    map_dict = spawn_network_map_command(map_cmd, map_options, map_target_net)
    ## turn the network map into a graph with the pygraphviz module
    mapgraph = netmap2graph(map_dict, map_target_net)
    ## write the graph to file
    print(mapgraph.string())
    assert(out_fname[-4:] == '.dot')
    mapgraph.write(out_fname)
    ## tell the graph object to calculate the layout
    mapgraph.layout(prog='dot')
    ## write output to a few graphical file formats
    for format in ('pdf', 'svg', 'png'):
        rendered_fname = out_fname[:-4] + '.' + format
        mapgraph.draw(rendered_fname)
        print('wrote graph to %s' % rendered_fname)


def spawn_network_map_command(cmd, options, target_net):
    map_dict = {}
    sudo_prefix = ['sudo', '-E'] if '-O' in options else []
    full_cmd = sudo_prefix + [cmd] + options + [target_net]
    print(full_cmd)
    p = subprocess.Popen(full_cmd, stdout=subprocess.PIPE)
    output_text, retval = p.communicate()
    output_text = output_text.decode('utf-8')
    print(retval)
    print(output_text)
    output_chunks = output_text.split('\n\n')
    for chunk in output_chunks:
        print('chunk:', chunk)
        if chunk.find('Nmap done:') != -1: # done processing chunks
            break
        fields_dict = get_fields(chunk)
        pprint.pprint(fields_dict)
        assert('ip' in fields_dict)
        assert(not fields_dict['ip'] in map_dict)
        map_dict[fields_dict['ip']] = fields_dict
    return map_dict


def get_fields(chunk):
    fields = {}
    lines = chunk.split('\n')
    # first get the simpler fields that have a clear "key: val"
    # pattern
    for key in ('Running', 'OS details', 'Network distance', 'MAC Address'):
        add_field_if_present(chunk, key, fields)
        print('---', key, '---')
        pprint.pprint(fields)
    # now get the special handling fields
    for line in lines:
        print('line:', line)
        keystring = 'Nmap scan report for'
        if line[:len(keystring)] == keystring:
            valstr = line[len(keystring):].strip()
            print('    valstr:', valstr, valstr.split()[0].strip())
            if valstr.find('(') == -1:
                host = ''
                ip = valstr
            else:
                host = valstr.split()[0].strip()
                ip = valstr.split()[1].strip().strip('()')
            fields['host'] = host
            fields['ip'] = ip
    return fields


def add_field_if_present(chunk, key, fields_dict):
    """If the nmap output has key in a clearly recognizable field position
    (i.e. if a line begins with key and then a colon) then we extract
    the "value" from that line by taking all the text after the colon
    and add it to the fields_dict.  If the key: is not present then we
    don't add anything to the fields_dict.  We usually get key: fields
    when we run nmap with the -O option.  For example, if the line is
    "Running: Linux 3.X|4.X" then we will add the (key, value) pair
    ("Running", "Linux 3.X|4.X") to fields_dict.
    """
    assert(not key in fields_dict)
    pos_start = 1 + chunk.find('\n' + key + ':')
    if pos_start == -1:
        return
    pos_end = chunk[pos_start:].find('\n')
    val = chunk[pos_start+len(key)+1 : (pos_start+pos_end)].strip()
    assert(not val in fields_dict)
    fields_dict[key] = val
    

def netmap2graph(map_dict, target_net):
    """take a network map dictionary and turn it in to a graphviz graph"""
    graph = pgv.AGraph()
    # graph.graph_attr['rankdir'] = 'TB' # use 'LR' for left-to-right
    graph.graph_attr['layout'] = 'twopi' # this makes a big circle surrounding a central area
    graph.graph_attr['ranksep'] = 3      # so the nodes don't trample each other
    graph.graph_attr['ratio'] = 'auto'
    graph.graph_attr['label'] = 'map of network %s' % target_net
    print('map_dict:')
    pprint.pprint(map_dict)
    ip_base_label = 'Net ' + target_net
    graph.add_node(ip_base_label, color='green', style='filled')
    for ip in natsorted(map_dict.keys()):
        if ip == 'base':
            # graph.add_edge(ip, 'Net ' + ip)
            graph.add_node('Net: ' + target_net, color='green', style='filled')
            # graph.add_edge(ip, 'Net ' + ip)
            continue
        # graph.add_node(ip)
        graph.add_node(ip, color='red', style='filled')
        # graph.add_edge(ip, 'base')
        graph.add_edge(ip, ip_base_label)
    return graph
    

# def get_ip():
#     import socket
#     s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#     try:
#         # doesn't even have to be reachable
#         s.connect(('10.255.255.255', 1))
#         IP = s.getsockname()[0]
#     except:
#         IP = '127.0.0.1'
#     finally:
#         s.close()
#     return IP


if __name__ == '__main__':
    main()
