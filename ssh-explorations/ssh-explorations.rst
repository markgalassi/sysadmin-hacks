==================
 ssh explorations
==================

hosts, accounts, simple ssh
===========================

set up two new VMs
------------------

in my case one with ubuntu 14.04, the other with ubuntu 16.04.  Call
the hosts tclient and tserver (test client, test server).


create entries in /etc/hosts for u1404 and u1604
------------------------------------------------

You do this by adding entries below the boiler-plate "localhost"
stuff.  In my case the end of my /etc/hosts files has these two lines:

.. code-block:: text

   192.168.122.88 tclient
   192.168.122.240 tserver

create a user on each VM called "ts" (test ssh):
------------------------------------------------

.. code-block:: console 

   $ sudo adduser ts

and for the purpose of this testing you can give it a quick password
to type: this will all be erased when the experiment is over.

experiment with simple ssh between hosts
----------------------------------------

All these tests will be done by logging in on the VM console, rather
than from ssh-ing in to the VMs with a terminal on your own console.

* on tclient:

  .. code-block:: console

     $ ssh tserver

* on tserver:

  .. code-block:: console

     $ ssh tclient

In both cases you should be prompted for a password and it should then
let you log in to the other host.

Now log out of all those ssh sessions.

Making ssh private/public key pairs, default (i.e. with-passphrase)
===================================================================

Log out and log back in as ts on the console of both client and
server.  Then:

Prepare the private/public key pairs
------------------------------------

On the client run:

.. code-block:: console

   $ ssh-keygen

and give it a passphrase.

Test ssh from client to server
------------------------------

Redo the simple ssh from client to server and then log out:

  .. code-block:: console

     tclient$ ssh tserver
     (give password)
     tserver$ exit

Test ssh to localhost
---------------------

ssh to localhost on client and then log out:

.. code-block:: console

   tclient$ ssh localhost
   (give password)
   tclient$ exit

The last two exercises show that although we have created keys,
they are not being used: it asks for password, not passphrase!
This is because neither client nor server has an authorized_keys
file.

Create authorized_keys on the server
------------------------------------

From tclient copy the ``id_rsa.pub`` to the server, then pull it into
the server's authorized_keys:

.. code-block:: console

   tclient$ scp ~/.ssh/id_rsa.pub tserver:/tmp/

.. code-block:: console

   tserver$ mkdir ~/.ssh
   tserver$ chmod 700 ~/.ssh
   tserver$ cat /tmp/id_rsa.pub >> ~/.ssh/authorized_keys
   tserver$ chmod 600 ~/.ssh/authorized_keys

Try again to log in from client into server
-------------------------------------------

.. code-block:: console

   tclient$ ssh tserver

This is now crucial: tclient should get a desktop-level prompt
asking you for your passphrase.  If not something is wrong and we
have to look at it.  Enter your passphrase.

You should now be logged in to tserver.

See if the ssh agent is working well for your next login
--------------------------------------------------------

Do two experiments on tclient:

* in the window you just used, exit from tserver and repeat:

  .. code-block:: console

     tclient$ ssh tserver

* in a new terminal also log in to tserver:

  .. code-block:: console

     tclient$ ssh tserver

  What you learn from these last two experiments is that the
  passphrase has been applied to the entire environment of this login
  session.


part III
========

FIXME: not yet written

