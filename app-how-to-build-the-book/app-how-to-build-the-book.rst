.. _app-how-to-build-the-book:

================================
 Appendix: How to build the book
================================

Motivation, prerequisites, plan
===============================

Our goal here is to show you how to build the book from its raw text
into the HTML and other formats in which the book is published.

We use the Sphinx typesetting system bla bla python docs use Sphinx
bla bla FIXME

Prerequisites:

* Installing the tools needed to clone and build the book.

* Learning how to use the Sphinx documentation system.

The tools needed
================

We need tools to get the book from the bitbucket network repository
(mercurial), and to build the book (sphinx).

.. code:: bash

   sudo apt install mercurial
   sudo apt install gnuplot5-qt ## or gnuplot-qt on newer versions of ubuntu
   sudo apt install python3-sphinx
   ## install the LaTeX packages needed to build the printable book
   ## with "make latexpdf"
   sudo apt install texlive-latex-base texlive-latex-recommended 
   sudo apt install python3-sphinx-rtd-theme texlive-latex-extra
   sudo apt install texlive-generic-extra texlive-fonts-recommended dot2tex
   sudo apt install biber latexmk
   ## NOTE: the plantuml extension is used in other books, not in this
   ## sudo apt install plantuml
   ## sudo apt install python3-sphinxcontrib.plantuml
   sudo apt install python3-pip
   sudo chown -R $LOGNAME /usr/local/lib/python*
   pip3 install sphinxcontrib.bibtex
   pip3 install sphinxcontrib.programoutput


Version control: cloning the repository (you only do this once)
===============================================================

We use mercurial to access the book repository.  Anyone can do so
with:

.. code:: bash

   hg clone http://hg@bitbucket.org/markgalassi/sysadmin-hacks

This will work for anyone.  If you are a collaborator with write
permission to the repository then you can clone it with a slightly
different URL:

.. code:: bash

   hg clone ssh://hg@bitbucket.org/markgalassi/sysadmin-hacks

You can then change in to the directory into which you have cloned the
repository, and examine what you have, with:

.. code:: bash

   $ cd sysadmin-hacks
   $ ls
   $ find . -name '*.rst'

That last ``find`` command will show you all the files that end in
``.rst``.  These files contain the text of the book written as simple
text with the light-weight *markdown* annotations.  Examine the files.


Building the book
=================

The sphinx documentation system will build the book in either HTML or
EPUB format.  The details are all taken care of and you can do so by
typing:

.. code:: bash

   $ cd sysadmin-hacks
   $ make html           ## (or "make epub")

You can then point your browser to the file ``build/html/index.html``
to see what it all looks like.


Making and committing changes (your day-to-day)
===============================================

You can make changes to the ``.rst`` files.  When you do so you need
to re-run ``make html`` and then view the changes by reloading the
HTML files in your browser.

First you should incorporate changes made by other people.  You type:

.. code:: bash

   $ hg pull
   $ hg update

Then you can make some changes to files, after which you can *commit*
them to the mercurial repository with

.. code:: bash

   $ hg commit

This commit will open an editor so that you can put in a log message.
This message should be thoughtful and describe briefly what changes
you made to each file you modified.

If you are a collaborator on the book and have write permission on
bitbucket you can now push your changes for other collaborators to see
with:

.. code:: bash

   $ hg push

(If you do not have write permission you can contact the authors to
get your changes to them.)

So the *workflow* is:

* Pull and update from bitbucket.

* Make changes to the book by modifying the ``.rst`` files.

* Commit the changes with ``hg commit``

* If you have write permission on bibucket, push in the code with ``hg
  push``
