.. _chap-creating-virtual-machines:

===========================
 Creating virtual machines
===========================

Motivation and plan
===================

Things are converging to where we will always deploy on virtual
machines ...

Plan: heavy reliance on the number 3.

* Choose VM guest OSes (example: Debian unstable, Ubuntu 16.04,
  CentOS 7)

* Choose up to three "killer apps" -- web-based applications to run
  off of these machines (examples: OwnCloud, ArchivesSpace, Trac,
  Jenkins, MediaWiki, Snipe-IT, a simple Django backend you can write
  up, a backup server...)


Physical base
=============

We write these instructions using Ubuntu 16.04 as the host operating
system.  This can obviously be changed.

You host the VMs on your own computer.  Make sure you have enough RAM
and hard disk space to handle creating one to three VMs.

Also make sure that hardware virtualization support is set.  This is
special hardware 

.. code-block:: console

   egrep '(vmx|svm)' --color=always /proc/cpuinfo

You should see strings with "vmx" or "svm" in them.  If you don't it
means that your hardware is not configured to support virtualization,
which means that running a VM would be very slow.

The place to configure this is in the BIOS settings: enable the option
for hardware virtualization support and reboot and check again.


.. _sec-iso-images-for-the-guest-operating-system:

ISO images for the guest operating systems
==========================================

Our goal is to flexibly experiment with installing a variety of
operating systems.  Let's get some ISO images handy for this work.

Debian weekly snapshot "netinst" image, about 300 megabytes.

Ubuntu 16.04.4 LTS (Xenial Xerus), about 1.5 gigabytes.  Let's also
get the server image, which lends itself better to automated
installation and doesn't have unnecessary software.  The server image
is about 850 meg.

For good measure, also get the analogous Ubuntu 18.04 images (Bionic
Beaver).

CentOS 7 "DVD" ISO, about 4 gigabytes.

You can download them with the following instructions, which you can
paste in all together.

.. code:: bash

   sudo mkdir -p /usr/local/src
   sudo chown $LOGNAME /usr/local/src
   mkdir -p /usr/local/src/cd-images
   cd /usr/local/src/cd-images/
   wget --continue https://cdimage.debian.org/cdimage/weekly-builds/amd64/iso-cd/debian-testing-amd64-netinst.iso
   wget --continue http://releases.ubuntu.com/16.04/ubuntu-16.04.4-server-amd64.iso
   wget --continue http://releases.ubuntu.com/18.04/ubuntu-18.04-server-amd64.iso
   wget --continue http://centos.s.uw.edu/centos/7/isos/x86_64/CentOS-7-x86_64-DVD-1708.iso


.. _sec-install-vms-from-a-gui:

Install VMs from a GUI
======================

Let us first experiment with installing a VM using the virt-manager
GUI.  This should be analogous to such installations from other
virtual machine systems.

Start by installing virt-manager and qemu-kvm:

.. code-block:: console

   sudo apt install virt-manager qemu-kvm

Make sure that you add yourself to the group libvirtd, and then reboot
and log in again.  This will make sure that you are a member of
libvirtd when you log in again.

Now, as yourself, run:

.. code-block:: console

   virt-manager &

and use the GUI to create yourself a VM with, let's say, 2gig of RAM
and an 80gig hard drive.

Then use the GUI to point the virtual host's CDROM to
``/usr/local/src/cd-images/ubuntu-server-16.04.4-amd64.iso``

Now you can start the VM and install the operating system.  Then you
could exercise your new guest OS by following the instructions in some
of the other chapters of this book where we set up a variety of
services.



Automated installation
======================

[ added 2019-04-30: found this URL that seems to do it all
https://raymii.org/s/articles/virt-install_introduction_and_copy_paste_distro_install_commands.html

more info on kickstart files and proxies is at
http://vijayjt.blogspot.com/2017/03/kvm-automation.html

there are problems when you're behind a proxy.  this question puts it
well, but it has no answers yet:
https://serverfault.com/questions/941671/redirect-all-web-trafic-from-network-through-proxy-server-to-a-specific-url

]


virt-manager
------------

GUI installation is pleasant, but it is slow.  There is a program
``virt-install`` which does the actual "under the hood" job of
installing a VM.

https://linuxconfig.org/how-to-create-and-manage-kvm-virtual-machines-from-cli

A typical command to create a VM might look like this:

.. code-block:: console

   virt-install --name=linuxconfig-vm \
   --name debian-test \
   --vcpus=1 \
   --memory=1024 \
   --cdrom=/usr/local/src/cd-images/debian-testing-amd64-netinst.iso \
   --disk size=5 \
   --os-variant=debian8

to boot, stop and remove-and-cleanup:

.. code-block:: console

   virsh boot debian-teset
   virsh destroy debian-test
   virsh undefine debian-test

or like this:

.. code-block:: console

   qemu-img create -f qcow2 ./centos7.qcow2 8G
   virt-install --name centos7 --ram 1024 --disk path=./centos7.qcow2,size=8 --vcpus 1 --os-type linux --os-variant rhel7 --network network=default --graphics none --console pty,target_type=serial --location 'http://repo.miserver.it.umich.edu/centos/7.6.1810/os/x86_64/' --extra-args 'console=ttyS0,115200n8'

to boot, stop and remove-and-cleanup:

.. code-block:: console

   virsh boot centos7
   virsh destroy centos7
   virsh undefine centos7

The first approach is problematic because it does not carry out the
installation and you still have to do a lot by hand.  The second
approach has the same problem and in addition it installs entirely
from the web instead of from a local CD, so it will be unusable from a
slow network connection.

To really achive *unattended* installation look at the procedures in
:numref:`sec-unattended-ubuntu` and :numref:`sec-unattended-el7`


.. tip::

   (this tip is out of sequence; should be when we do ks/preseed) To
   generate an encrypted password to use in a kickstarter or preseed
   file you can do this:

   .. code-block:: console

      python -c 'import crypt,getpass; print(crypt.crypt(getpass.getpass(), crypt.mksalt(crypt.METHOD_SHA512)))'


Other sites:

* https://computingforgeeks.com/rhel-centos-kickstart-automated-installation-kvm-virt-install/
  (I'm trying this one right now)

* https://github.com/fubralimited/CentOS-KVM-Image-Tools

* https://raymii.org/s/articles/virt-install_introduction_and_copy_paste_distro_install_commands.html


virtualbox
----------

Preparation: 

Installation from GUI

Automated installation


Unattended installation of some operating systems
=================================================

Concepts
--------

The parts of an unattended installation are:

* Preparing a hard disk image.

* Invoking virt-install with the following parameters:

  * A naming of the VM within virt-manager.

  * Memory.

  * Injection of a kickstarter or preseed file.

  * Location *from which to install* the OS.  This can be a CD image or
    a web location or a location in the filesystem.

* A specification of how to answer the installation questions.  On
  debian-based systems (such as debian and ubuntu) this is in a file
  called ``preseed.cfg``, while on redhat-based systems it's in a
  *kickstarter* file which often ends in ``.ks``

The hard disk image can be prepared with the ``qemu-img create``
command.  An example of the syntax is ``qemu-img create -f qcow2
$DISK_IMAGE 100G``.

The naming of the VM is done with the ``--name`` option, and RAM with
the ``--memory`` option.

The location from which to install is specified with either the
``--cdrom`` option or the ``--location`` option.  There are subtle
differences and rules on how to use these options, so I usually figure
out a recipe and just follow it.

.. _sec-unattended-ubuntu:

Unattended Ubuntu installation
------------------------------

I usually use the script ``make-minimal-ubuntu.sh`` in
:numref:`listing-make-minimal-ubuntu` to make a new unattended ubuntu
VM.  This will make use of a *preseed* file.

.. _listing-make-minimal-ubuntu:

.. literalinclude:: make-minimal-ubuntu.sh
   :language: bash
   :caption: Make a minimal ubuntu host, unattended.  Note that the
             script first removes the resources of a previous host by
             that name, then creates a disk image, then calls
             virt-install to install the VM.  It also uses
             ``preseed.cfg``, shown in :numref:`listing-preseed-cfg`.

.. _listing-preseed-cfg:

.. literalinclude:: preseed.cfg
   :language: bash
   :caption: preseed.cfg file for unattended minimal ubuntu 16.04
             installation.

You now have a new VM, ready to work with, which was installed with no
human intervention after the ``virt-install`` command.  It's running
ubuntu 16.04.4

You will find this VM in the virt-manager list of VMs.  You can bring
up its console and log in to find out what the IP address is, and to
install openssh-server.

.. _sec-unattended-el7:

Unattended CentOS7/RHEL7 installation
-------------------------------------

[incomplete; I have the minimal-el7.ks file and the
make-minimal-el7.sh files, but I have not yet fixed the write-up in
this section.]

After a minimal install of CentOS7 you don't even have the network
coming up automatically.  Log in as root and do:

.. code-block:: console

   sed -i 's/ONBOOT=no/ONBOOT=yes/' /etc/sysconfig/network-scripts/ifcfg-ens3
   reboot

Then log in as root again and you will have a network.  Install some
basic s/w before following the procedure for Archivesspace.

.. code-block:: console
                
   ## for good measure, update the system
   yum update -y
   sudo yum install -y net-tools curl wget openssh-server spice-vdagent
   sudo yum install -y telnet zip unzip lynx links
   ifconfig  ## then take not of our ip address

At this point you can bypass the console and ssh in from your
workstation, which is more convenient, with:

.. code-block:: console
                
   ssh root@ip_address_found_with_ifconfig

Then log in from a terminal on your own workstation.

.. code-block:: console
                
   ssh ip_address_found_with_ifconfig


Install Apache, PHP and MariaDB
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

and on the CentOS7 VM we now prepare the stack:

.. code-block:: console

   sudo yum -y install epel-release
   sudo yum -y update
   sudo shutdown -r now
   ## after reboot, ssh back in and do:
   sudo yum -y install httpd
   ## Start Apache and enable it to automatically run at boot time.
   sudo systemctl start httpd.service
   sudo systemctl enable httpd.service
