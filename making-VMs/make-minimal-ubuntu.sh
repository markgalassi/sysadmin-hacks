#! /bin/sh

#HOST_ID=unattendedU
HOST_ID=tryu3

virsh destroy ${HOST_ID}
virsh undefine ${HOST_ID}

## ubuntu does not need us to mount the ISO file, so we just point to
## the ISO
INSTALL_ISO=/usr/local/src/cd-images/ubuntu-16.04.4-server-amd64.iso
#INSTALL_ISO=/usr/local/src/cd-images/ubuntu-18.04-live-server-amd64.iso
#INSTALL_ISO=/usr/local/src/cd-images/ubuntu-18.04-desktop-amd64.iso
#INSTALL_ISO=/usr/local/src/cd-images/ubuntu-18.04-mini.iso

DISK_IMAGE=/data1/vm-images/${HOST_ID}.qcow2
/bin/rm -f $DISK_IMAGE
qemu-img create -f qcow2 $DISK_IMAGE 100G

sudo virt-install \
--name ${HOST_ID} \
--vcpus 2 \
--memory 4096 \
--disk size=100,bus=virtio,format=qcow2 \
--boot cdrom,hd \
--network network=default \
--graphics vnc \
--location $INSTALL_ISO \
--initrd-inject=preseed.cfg \
--extra-args="auto=true netcfg/use_autoconfig=true netcfg/disable_dhcp=false netcfg/get_hostname=${HOST_ID} file=file:/preseed.cfg"
