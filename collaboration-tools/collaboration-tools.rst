================================
 Setting up collaboration tools
================================

* corporate chat (IRC and modern such programs)



IRC-like
========

* matrix-synapse: seems to not have too many steps - no database as
  far as I can tell
  https://www.howtoforge.com/tutorial/how-to-install-matrix-synapse-on-ubuntu-1804/

* mattermost: seems to have a ton of steps on ubuntu, including a
  mysql setup
  https://docs.mattermost.com/install/install-ubuntu-1804.html

* "let's chat": seems to have lots of steps, including mongodb setup
  https://www.tecmint.com/install-lets-chat-on-centos-ubuntu-debian/
