#! /bin/sh

HOST_ID=tryu2

virsh destroy ${HOST_ID}
virsh undefine ${HOST_ID}

## ubuntu does not need us to mount the ISO file, so we just point to
## the ISO
INSTALL_ISO=/usr/local/src/cd-images/ubuntu-16.04.4-server-amd64.iso

# ISO_MNT_DIR=$HOME/mnt/ubuntu-iso-dir
# mkdir -p $ISO_MNT_DIR
# sudo mount -t iso9660 -o loop /usr/local/src/cd-images/ubuntu-16.04.4-desktop-amd64.iso $ISO_MNT_DIR

DISK_IMAGE=/data1/vm-images/${HOST_ID}.qcow2
/bin/rm -f $DISK_IMAGE
qemu-img create -f qcow2 $DISK_IMAGE 100G

sudo virt-install \
--name ${HOST_ID} \
--vcpus 2 \
--memory 4096 \
--disk size=100,bus=virtio,format=qcow2 \
--boot cdrom,hd \
--network network=default \
--graphics vnc \
--location $INSTALL_ISO \
--initrd-inject=preseed.cfg \
--extra-args="auto=true netcfg/use_autoconfig=true netcfg/disable_dhcp=false netcfg/get_hostname=${HOST_ID} file=file:/preseed.cfg file=file:/snipeit-substituted.sh"

## --location /usr/local/src/cd-images/ubuntu-16.04.4-desktop-amd64.iso
## --location $ISO_MNT_DIR


# virt-install \
# --name ${HOST_ID} \
# --ram 4096 \
# --disk path=$DISK_IMAGE,size=8 \
# --vcpus 1 \
# --os-type linux \
# --os-variant ubuntu \
# --network network=default \
# --graphics vnc \
# --console pty,target_type=serial \
# --location http://us.ubuntu.com/dists/xenial/main/installer-amd64 \
# --initrd-inject preseed.cfg \
# --extra-args "file=file:/preseed.cfg"

## NOTE: for debian make it a preseed.cfg file
#https://wiki.debian.org/DebianInstaller/Preseed
#http://honk.sigxcpu.org/projects/libvirt/preseed/preseed.cfg
