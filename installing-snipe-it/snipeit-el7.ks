## minimal el7 kickstart file based on the one found at
## https://www.centosblog.com/centos-7-minimal-kickstart-file/

install
lang en_US.UTF-8
keyboard us
timezone America/Denver
auth --useshadow --enablemd5
selinux --disabled
firewall --disabled
services --enabled=NetworkManager,sshd
## eula --agreed
ignoredisk --only-use=sda
reboot

bootloader --location=mbr
zerombr
clearpart --all --initlabel
part swap --asprimary --fstype="swap" --size=1024
part /boot --fstype ext4 --size=200
part pv.01 --size=1 --grow
volgroup rootvg01 pv.01
logvol / --fstype ext4 --name=lv01 --vgname=rootvg01 --size=1 --grow

#rootpw --iscrypted $YOUR_ROOT_PASSWORD_HASH_HERE
rootpw as123

 repo --name=base --baseurl=http://mirror.cogentco.com/pub/linux/centos/7/os/x86_64/
# repo --name=base --baseurl=file:///home/markgalassi/repo/centos7-iso-dir/
url --url="http://mirror.cogentco.com/pub/linux/centos/7/os/x86_64/" --proxy=http://proxyout.lanl.gov:8080/
# url --url="file:///home/markgalassi/mnt/centos7-iso-dir/" --proxy=http://proxyout.lanl.gov:8080/
#url --url="http://mirror.cogentco.com/pub/linux/centos/7/os/x86_64/"

%packages --nobase --ignoremissing
@core
net-tools
curl
wget
openssh-server
%end


%post
setenforce 0
echo 'proxy=http://proxyout.lanl.gov:8080/' >> /etc/yum.conf
yum -y install epel-release
mkdir -p /var/www/html
cd /var/www/html/
export HTTP_PROXY=http://proxyout.lanl.gov:8080/
export http_proxy=http://proxyout.lanl.gov:8080/
export HTTPS_PROXY=http://proxyout.lanl.gov:8080/
export https_proxy=http://proxyout.lanl.gov:8080/
wget https://raw.githubusercontent.com/snipe/snipe-it/master/install.sh
chmod 744 install.sh
#./install.sh
#sed -i "s/'timezone' => '',/'timezone' => 'UTC',/" snipeit/config/app.php
#cd snipeit
### not sure about this one, but what they had did not work with the
### current version of snipeit
#php artisan app:name snipeit
#firewall-cmd --zone=public --add-port=80/tcp --permanent
#firewall-cmd --reload
%end
