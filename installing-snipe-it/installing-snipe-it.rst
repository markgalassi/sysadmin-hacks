.. _sec-installing-snipe-it:

=====================
 Installing Snipe-IT
=====================

Motivation, prerequisites and plan
==================================

.. rubric:: Motivation

Snipe-IT is an ambitious piece of web-based software which allows
users to track IT assets.

It uses PHP for its back end, as well as various frameworks that run
on top of PHP.  It also requires installation and configuration of the
apache web server, as well as a MySQL database.

We want to come up with a procedure that lets us prepare virtual
machines (VMs) with Snipe-IT.  Apart from the usefulness of Snipe-IT
(which might not be for everybody), this will illustrate how one sets
up a complex web server.

.. rubric:: Prerequisites

Prepare a machine to run Snipe-IT.  This can be done on any machine,
but it's useful to do this on a brand new VM.

To make this VM you can follow the procedure in
:numref:`chap-creating-virtual-machines`, specifically the "unattended
Ubuntu installation" :numref:`sec-unattended-ubuntu`.

.. rubric:: Plan

I will illustrate what I think is the most robust installation
procedure, and then show two of the procedures from the official
Snipe-IT installation instructions.  These last two try to do it all
with a single instruction, but they are not robust and it ends up
being difficult to fix the problems that come up.


The procedure that works most robustly
======================================

The most robust procedure is from the Ceos3c Tutorials.  It's called
"How to install LAMP on Ubuntu 16.04" and can be found at
https://www.ceos3c.com/open-source/install-lamp-ubuntu-16-04/

This will have us take two steps: first install the LAMP stack, and
then install Snipe-IT itself.  Before we start, for good measure, let
us upgrade our system.

Log in to the VM, for example with ``ssh ubuntu@192.168.122.NUMBER``

Remember that the procedure in :numref:`sec-unattended-ubuntu` sets
login and password to "ubuntu".  You should change these for
deployment, of course, but for now just ``ssh`` in to the VM with that
account.

In this tutorial I will use "ubuntu" as login and password to the
GNU/Linux host, and "snipeit" as the login and password for everything
else (mostly for the MySQL database).

First, for good measure, run:

.. code-block:: console

   sudo apt update -y && sudo apt dist-upgrade -y
   sudo apt install -y wget curl git openssh-server

Then follow the steps in the next sections.

Installing the LAMP stack
-------------------------

LAMP used to stand for "Linux, Apache, MySQL, Perl".  Today Perl has
fallen out of favor as a back end language, but the acronym still
works because in many applications it has been replaced by PHP, so the
initials still work.

Installing the *LAMP stack* is the prerequisite for many packages,
including this one.

We will follow the instructions here:

https://www.ceos3c.com/open-source/install-lamp-ubuntu-16-04/

and I will reproduce them here, but with a bit less explanation.  You
should definitely follow the tutorial at least once to see some of
their extra explanations.

.. code-block:: console

   sudo apt-get install apache2

the tutorial now wants you to verify that the apache2 installation was
successful by bringing up http://localhost/ in a browser.  At this
time you are probably running the browser from your main host, not
from the VM, so it will not see localhost.  You can verify it with the
terminal-based browser lynx with ``lynx http://localhost/`` and see if
you get the generic apache greeting page.

Continue installing:

.. code-block:: console

   sudo apt install mysql-server

For now, to make this procedure quick and reproducible, use "snipeit"
for login and password in the MySQL setup questions.

Then secure it with

.. code-block:: console

   sudo mysql_secure_installation
   ## and answer with: snipeit, N, N, Y, Y, Y, Y

Now install PHP:

.. code-block:: console

   sudo apt-get install -y php libapache2-mod-php php-mysql php-curl php-gd php-intl php-pear php-imagick php-imap php-mcrypt php-memcache php-pspell php-recode php-sqlite3 php-tidy php-xmlrpc php-xsl php-mbstring php-gettext

and restart apache2 with:

.. code-block:: console

   sudo /etc/init.d/apache2 restart

To test PHP put this single line into ``/var/www/html/phpinfo.php``:

.. code-block:: php

   <?php phpinfo(); ?>

You can now test it again with ``lynx http://localhost/phpinfo.php``
to see if PHP is properly set up.

We're done with the LAMP stack installation.


Installing Snipe-IT with the Coes3c Tutorial procedure
------------------------------------------------------

Now return to the main procedure in
https://www.ceos3c.com/cloud/how-to-install-snipeit-on-ubuntu-16-04-on-aws-free-tier/

We go in as root and run the following commands to install composer
and download snipe-it from github:

.. code-block:: console

   sudo -i
   curl -sS https://getcomposer.org/installer | php
   mv composer.phar /usr/local/bin/composer
   cd /var/www
   git clone https://github.com/snipe/snipe-it snipeit
   cd snipeit/
   cp .env.example .env

Next we configure the MySQL database.  Remembering that at this time
we have set the password to ``snipeit``, enter the client with:

.. code-block:: console

   $ mysql -u root -p
   mysql> create database snipeit;
   mysql> CREATE USER 'snipeit'@'localhost' IDENTIFIED BY 'snipeit';
   mysql> GRANT ALL PRIVILEGES ON snipeit.* TO 'snipeit'@'localhost';
   mysql> flush privileges;
   mysql> exit


Now that the database is ready we connect the Snipe-IT software to the
database via the ``.env`` file.

Edit ``/var/www/snipeit/.env`` and only change these fields:

* APP_URL becomes your IP address, for example 192.168.122.NUMBER

* DB_DATABASE becomes snipeit

* DB_USERNAME becomes snipeit

* DB_PASSWORD becomes snipeit

and that's everything you need to do here.

.. note::

   We have left the ``DB_HOST`` to ``127.0.0.1`` (which is the same as
   ``localhost``) instead of changing it to the IP address.  Why do we
   sometimes use the full IP address and sometimes we're OK with
   ``localhost``?  Here's the distinction: when Snipe-IT talks to it's
   MySQL database, we're guaranteed that it's on the same host, so
   ``localhost`` is fine. Instead when we configure apache2 we will
   want it to be available from outside, so we will use the full IP
   address.


Now let's finalize some permissions.  Remembering that we are still
root, let's type:

.. code-block:: console

    chown -R www-data:www-data storage public/uploads
    chmod -R 775 storage
    chmod -R 755 public/uploads

Some more PHP:

.. code-block:: console

   apt-get install -y git unzip php php-mcrypt php-curl php-mysql php-gd php-ldap php-zip php-mbstring php-xml
   apt-get install php7.0-bcmath

Now the *very lengthy* step of installing ``composer``.  This will
grab a bunch of other necessary stuff.

.. code-block:: console

   composer install --no-dev --prefer-source

Look closely at the output when this finishes, and scroll back up.
One of the packages that gets downloaded is psysh, and this one takes
so long that it sometimes times out.  There is a way to lengthen the
timeout, but I have not yet written it up.

Now generate your key with:

.. code-block:: console

   php artisan key:generate
   ## type yes when asked.  Save the key somewhere, though it will
   ## also be pasted into your .env file.

The last thing we need to do is set up the apache2 configuration.  We
will edit the file
``/etc/apache2/sites-available/snipeit.example.com.conf`` and put this
content in it:

.. code-block:: text

   <VirtualHost *:80>
     ServerAdmin webmaster@localhost
     <Directory /var/www/snipeit/public>
       Require all granted
       AllowOverride All
     </Directory>
     DocumentRoot /var/www/snipeit/public
     ServerName YOURSERVERIP
     #Redirect permanent / https://snipeit.your-server-fqdn.com/
     ErrorLog /var/log/apache2/snipeIT.error.log
     CustomLog /var/log/apache2/access.log combined
   </VirtualHost>

where the only thing you must change is YOURSERVERIP is your VM's IP
address, for example in a virt-manager situation it could be
192.168.122.NUMBER

Last few apache commands:

.. code-block:: console

   sudo a2ensite snipeit.example.com.conf
   sudo a2enmod rewrite
   sudo systemctl restart apache2
   ## disable the default.conf
   sudo a2dissite 000-default.conf
   sudo service apache2 restart
   ## now look at apache2's ``sites-available`` area:
   cd /etc/apache2/sites-available
   ## rename the default.conf to keep it as a backup
   sudo cp 000-default.conf 000-default.confTEMP
   ## remove it
   sudo rm 000-default.conf
   ## final apache/php commands:
   sudo phpenmod mcrypt
   sudo phpenmod mbstring
   sudo a2enmod rewrite
   sudo service apache2 restart

From here on it's all done with the web browser GUI.  On your main
host open a browser window to the URL ``http://YOURIPADDRESS/`` (for
example ``http://192.168.122.NUMBER/`` )

And we're done with this procedure: the GUI should be
self-explanatory.




[obsolete] Installing Snipe-IT on debian/ubuntu
===============================================

https://www.howtoforge.com/tutorial/how-to-install-snipe-it-on-debian-9/

https://www.tecmint.com/install-snipe-it-asset-management-on-centos-ubuntu-debian/

[obsolete] Preparing a bootable ISO
===================================

Some links:

https://access.redhat.com/discussions/1422213

http://www.frankreimer.de/?p=522

https://www.golinuxhub.com/2017/05/how-to-create-customized-bootable-boot.html

http://www.smorgasbork.com/2012/01/04/building-a-custom-centos-6-kickstart-disc-part-1/

http://www.smorgasbork.com/2012/01/04/building-a-custom-centos-7-kickstart-disc-part-2/
