#! /bin/sh

HOST_ID=try2

virsh destroy ${HOST_ID}
virsh undefine ${HOST_ID}

ISO_MNT_DIR=$HOME/mnt/centos7-iso-dir
mkdir -p $ISO_MNT_DIR
sudo mount -t iso9660 -o loop /usr/local/src/cd-images/CentOS-7-x86_64-DVD-1708.iso $ISO_MNT_DIR
DISK_IMAGE=/data1/vm-images/${HOST_ID}.qcow2

/bin/rm -f $DISK_IMAGE
qemu-img create -f qcow2 $DISK_IMAGE 100G

virt-install \
--name ${HOST_ID} \
--ram 4096 \
--disk path=$DISK_IMAGE,size=8 \
--vcpus 1 \
--os-type generic \
--os-variant generic \
--network network=default \
--graphics vnc \
--console pty,target_type=serial \
--location $ISO_MNT_DIR \
--initrd-inject snipeit-el7.ks \
--initrd-inject snipeit-substituted.sh \
--extra-args "ks=file:/snipeit-el7.ks"
