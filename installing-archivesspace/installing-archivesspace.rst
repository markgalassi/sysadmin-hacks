.. _sec-installing-archivesspace:

==========================
 Installing Archivesspace
==========================

Motivation and plan
===================

A useful piece of software for ...

We want to come up with a procedure that lets us prepare ready-to-go
VMs with Archivesspace.

Prerequisites
=============

Preparing CentOS7
-----------------

First install CentOS7 and some tools as shown in
:numref:`sec-unattended-el7`

At this point you can bypass the console and ssh in from your
workstation, which is more convenient, with:

.. code-block:: console
                
   ssh root@ip_address_found_with_ifconfig

As of 2018-04-21 the verseion of java to use is 1.8.0:

.. code-block:: console

   sudo yum install java-1.8.0-openjdk-headless -y

Preparing ubuntu 16.04
----------------------

Prepare an ubuntu 16.04 VM following the directions in
:numref:`sec-install-vms-from-a-gui` or
:numref:`sec-unattended-ubuntu`.  Install a couple of needed utilities
on top of the bare-bones ubuntu 16.04:

.. code-block:: console

   sudo apt install -y zip unzip openssh-server wget curl

At this point you can bypass the console and ssh in from your
workstation, which is more convenient, with:

.. code-block:: console
                
   ssh LOGIN_NAME@ip_address_found_with_ifconfig
   ## LOGIN_NAME should be replaced with your login name
   ## on that computer

Then install java 1.8.0 with:

.. code-block:: console

   sudo apt install -y openjdk-8-jre-headless

.. _sec-download-archivesspace:

Download archivesspace
----------------------

First verify that the procedures above got us the correct version of
java.  Following the "getting started" page at:

http://archivesspace.github.io/archivesspace/user/getting-started/

.. code-block:: console

   java -version

From https://github.com/archivesspace/archivesspace/releases

For example, as root do:

.. code-block:: console

   sudo mkdir -p /opt
   sudo chown $LOGNAME /opt
   ## note that LOGNAME is an automatic shell variable, so
   ## leaving $LOGNAME here will replace it with your
   ## login name
   cd /opt/
   wget https://github.com/archivesspace/archivesspace/releases/download/vX.Y.Z/archivesspace-vX.Y.Z.zip
   ## note that X.Y.Z is the actual version of archivesspace.  You
   ## should find on their web site which is the most recent one
   ## and replace X, Y and Z with those major/minor/patch version
   ## numbers

Finally: install Archivesspace
------------------------------

Continuing with the procedure at
http://archivesspace.github.io/archivesspace/user/getting-started/ we
now run:

.. code-block:: console

   cd /opt/
   unzip archivesspace-vX.Y.Z.zip
   cd archivesspace
   ./archivesspace.sh

This takes a very long time to run.  After this you can log in to it
by pointing your browser to ``http://IP_ADDR:8089/`` or the various
other ports 8080, 8081, 8082, 8090.

More detail from the archivesspace web site:

.. code-block:: text

    http://localhost:8089/ – the backend
    http://localhost:8080/ – the staff interface
    http://localhost:8081/ – the public interface
    http://localhost:8082/ – the OAI-PMH server
    http://localhost:8090/ – the Solr admin console


Configuring archivesspace
-------------------------

Some of the information I used here comes from:

http://archivesspace.github.io/archivesspace/user/configuring-archivesspace/

There are two things we might need to change in the default
configuration, which is stored in ``config/config.rb``

* This is common in internet services: they often come configured by
  default to listen to the "localhost" interface for debugging
  purposes.  This means that you would only be able to access the
  service from a browser on the same host.  The way to change it is
  often to switch where it says "localhost" to say the full IP
  address, or the fully qualified domain name for the host.  We will
  often not have a proper routed hostname, so the IP address will work
  well.

* By default archivesspace will use a sqlite database which needs no
  configuration.  You can switch it to use a MySQL database.

At this time we will do the first of those, but we will not change the
database.

Edit ``config/config.rb`` and change the lines that say localhost:8080
(and so forth) to say IP_ADDR:8080 (for example
192.168.122.NUMBER:8080).

You could try to automate this with something like:

.. code-block:: console

   export ip4=$(/sbin/ip -o -4 addr list ens3 | awk '{print $4}' | cut -d/ -f1)
   cp /opt/archivesspace/config/config.rb /opt/archivesspace/config/config.rb-orig
   cat /opt/archivesspace/config/config.rb-orig | sed "s/http:\/\/localhost:808/http:\/\/$ip4:808/g" > /opt/archivesspace/config/config.rb

Then open those ports with firewall-cmd, for example following
https://linuxconfig.org/how-to-open-http-port-80-on-redhat-7-linux-using-firewall-cmd
with:

.. code-block:: console

   firewall-cmd --zone=public --add-port=8080/tcp --permanent
   firewall-cmd --zone=public --add-port=8081/tcp --permanent
   firewall-cmd --zone=public --add-port=8082/tcp --permanent
   firewall-cmd --zone=public --add-port=8089/tcp --permanent
   firewall-cmd --zone=public --add-port=8090/tcp --permanent
   firewall-cmd --reload

Then check that they are open with:

.. code-block:: console

   iptables-save | grep 80

Now you can go from a browser on your console and go to those IP
addresses to log in and configure:

.. code-block:: text

    http://IP_ADDR:8089/ – the backend
    http://IP_ADDR:8080/ – the staff interface
    http://IP_ADDR:8081/ – the public interface
    http://IP_ADDR:8082/ – the OAI-PMH server
    http://IP_ADDR:8090/ – the Solr admin console


Using a MySQL database
======================

.. code-block:: console

   sudo apt install mysql-client
   sudo apt install mysql-server
   ## for our purposes here set the root password to as123

Following the procedure in:

http://archivesspace.github.io/archivesspace/user/running-archivesspace-against-mysql/

.. code-block:: console

   $ mysql -uroot -p

   mysql> create database archivesspace default character set utf8;
   Query OK, 1 row affected (0.08 sec)
   mysql> grant all on archivesspace.* to 'as'@'localhost' identified by 'as123';
   Query OK, 0 rows affected (0.21 sec)

Now that the database exists we connect archivesspace to it.  This is
done through the ``config/config.rb`` file: we uncomment the line with
``##AppConfig[:db_url] = [...]`` You can do that by hand, or with a
sed script:

.. code-block:: console

   cp /opt/archivesspace/config/config.rb /opt/archivesspace/config/config.rb-before-mysql
   cat /opt/archivesspace/config/config.rb-before-mysql | sed 's/^##AppConfig\(.*\)mysql/AppConfig\1mysql/' > /opt/archivesspace/config/config.rb

Now archivesspace knows what database and table and login and password
to use.  We now do the final connection:





Upgrading from Archivesspace 2.3.0 to Archivesspace 2.3.2
=========================================================

Upgrading archivesspace, which uses a database, is an interesting
exercise becuase it brings out some of the issues that affect all such
web services: *how to migrate the data to the new installation.*

Different web services will have different ways of migrating, but they
will all involve an approach for data migration, and this approach
is an important part of how the package was designed.

Let us set up a situation to study this migration/upgrade:

#. Create an Ubuntu VM (or use a computer or VM that you already
   have).  You can use the procedure in
   :numref:`sec-unattended-ubuntu`.

#. Install Archivesspace 2.3.0 using the procedure given in
   :numref:`sec-download-archivesspace` and the sections that follow
   that.


